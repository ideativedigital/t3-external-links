<?php
declare(strict_types=1);
namespace Cobweb\ExternalLinks\Controller;

/*
 * This file is part of the Cobweb/ExternalLinks project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use Cobweb\ExternalLinks\Builder\ExternalLinkBuilder;
use Cobweb\ExternalLinks\Domain\Repository\ExternalLinkRepository;
use Cobweb\ExternalLinks\Formatter\RecordsFormatter;
use Cobweb\ExternalLinks\Validator\ExternalLinkValidator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ExternalLinkAjaxController
 */
class ExternalLinkAjaxController
{

    /**
     * @param ServerRequestInterface $request the current request
     * @param ResponseInterface $response the current response
     * @return ResponseInterface the finished response with the content
     */
    public function saveAction(ServerRequestInterface $request): ResponseInterface
    {
        $result = false;
        $externalLink = $this->getExternalLinkBuilder()->build($request->getParsedBody());

        // Validate the data before saving anything
        if ($this->getExternalLinkValidator()->validate($externalLink)) {
            // If a uid was passed, we are editing an existing entry, otherwise creating a new one
            if (isset($externalLink['uid']) && $externalLink['uid'] > 0) {
                // Ensure the uid received correspond to an existing entry
                $existingExternalLink = $this->getExternalLinkRepository()->findByIdentifier($externalLink['uid']);
                if ($existingExternalLink) {
                    $result = $this->getExternalLinkRepository()->update($externalLink)
                        ? $externalLink
                        : false;
                    $action = 'update';
                }
            } else {
                $result = $this->getExternalLinkRepository()->create($externalLink)
                    ? $externalLink
                    : false;
                $action = 'create';
            }
        }

        if (is_array($result)) {
            $result['action'] = $action;
        }

        $response = GeneralUtility::makeInstance(Response::class);
        $response->getBody()->write(json_encode($result));
        return $response;
    }

    /**
     * @param ServerRequestInterface $request the current request
     * @param ResponseInterface $response the current response
     * @return ResponseInterface the finished response with the content
     */
    public function listAction(ServerRequestInterface $request): ResponseInterface
    {
		$records = $this->getExternalLinkRepository()->search(
			!empty($request->getQueryParams()['search']) ? urldecode($request->getQueryParams()['search']) : ''
		);

		// If there are no search criteria, no record found and a link already existing : set this link in list
		if (empty($request->getQueryParams()['search']) && empty($records)) {
			$uid = (int)$request->getQueryParams()['uid'];
			if ($uid) {
				$record = $this->getExternalLinkRepository()->findByIdentifier($uid);
				if (!empty($record)) {
					$records[] = $record;
				}
				else {
					$records = [];
				}
			}
		}

        $data = [
            'recordsTotal' => count($records),
            'data' => $this->getRecordsFormatter()->format($records)
        ];
        $response = GeneralUtility::makeInstance(Response::class);
        $response->getBody()->write(
            json_encode($data)
        );
        return $response;
    }

    /**
     * @param ServerRequestInterface $request the current request
     * @param ResponseInterface $response the current response
     * @return ResponseInterface the finished response with the content
     */
    public function deleteAction(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getParsedBody();

        $result = false;
        if (isset($data['uid']) && (int)$data['uid'] > 0) {

            $externalLink = $this->getExternalLinkRepository()->findByIdentifier((int)$data['uid']);
            if ($externalLink) {
                $result = $this->getExternalLinkRepository()->delete($externalLink);
            }
        }
        $response = GeneralUtility::makeInstance(Response::class);
        $response->getBody()->write(
            json_encode($result)
        );
        return $response;
    }

    /**
     * @return ExternalLinkRepository
     */
    protected function getExternalLinkRepository(): ExternalLinkRepository
    {
        return GeneralUtility::makeInstance(ExternalLinkRepository::class);
    }

    /**
     * @return RecordsFormatter
     */
    protected function getRecordsFormatter(): RecordsFormatter
    {
        return GeneralUtility::makeInstance(RecordsFormatter::class);
    }

    /**
     * @return ExternalLinkValidator
     */
    protected function getExternalLinkValidator(): ExternalLinkValidator
    {
        return GeneralUtility::makeInstance(ExternalLinkValidator::class);
    }

    /**
     * @return object|ExternalLinkBuilder
     */
    protected function getExternalLinkBuilder(): ExternalLinkBuilder
    {
        return GeneralUtility::makeInstance(ExternalLinkBuilder::class);
    }

}