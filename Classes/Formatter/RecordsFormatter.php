<?php
declare(strict_types=1);
namespace Cobweb\ExternalLinks\Formatter;

/*
 * This file is part of the Cobweb/ExternalLinks project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use Cobweb\ExternalLinks\RecordFormatterHookInterface;
use Cobweb\ExternalLinks\View\Button\DeleteButton;
use Cobweb\ExternalLinks\View\Button\EditButton;
use Cobweb\ExternalLinks\View\Record\RecordIcon;
use Cobweb\ExternalLinks\View\Record\RecordLabel;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Format a record to be displayed in the Grid in the LinkHandler popup.
 *
 * Class RecordsFormatter
 */
class RecordsFormatter
{
    /**
     * @var array List of available hooks
     */
    protected $hooks = [];

    public function __construct()
    {
        // Load the hooks (if they implement the proper interface)
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['external_links']['recordFormatter'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['external_links']['recordFormatter'] as $className) {
                $object = GeneralUtility::makeInstance($className);
                if ($object instanceof RecordFormatterHookInterface) {
                    $this->hooks[] = $object;
                }
            }
        }
    }

    /**
     * @param array $externalLinks
     * @return array
     */
    public function format(array $externalLinks): array
    {
        $formattedRecords = [];
        foreach ($externalLinks as $externalLink) {
            $formattedRecords[] = $this->formatOne($externalLink);
        }

        return $formattedRecords;
    }

    /**
     * @param array $externalLink
     * @return array
     */
    public function formatOne(array $externalLink): array
    {
        $renderEditButton = true;
        $renderDeleteButton = true;
        $buttons = '';

        // Let hooks modify buttons visibility (note: last hook takes precedence)
        /** @var RecordFormatterHookInterface $hook */
        foreach ($this->hooks as $hook) {
            $renderEditButton = $hook->displayEditButton($externalLink);
            $renderDeleteButton = $hook->displayDeleteButton($externalLink);
        }

        // Render the buttons
        if ($renderEditButton) {
            $buttons .= $this->getEditButton()->render($externalLink);
        }
        if ($renderDeleteButton) {
            if ($buttons !== '') {
                $buttons .= ' ';
            }
            $buttons .= $this->getDeleteButton()->render($externalLink);
        }
        $externalLink['commands'] = sprintf(
            '<div class="btn-toolbar pull-right" role="toolbar" aria-label=""><div class="btn-group" role="group" aria-label="">%s</div></div>',
            $buttons
        );
        $externalLink['label'] = $this->getRecordLabel()->render($externalLink);
        $externalLink['icon'] = $this->getRecordIcon()->render($externalLink);

        return $externalLink;
    }

    /**
     * @return object|EditButton
     */
    protected function getEditButton()
    {
        return GeneralUtility::makeInstance(EditButton::class);
    }

    /**
     * @return object|DeleteButton
     */
    protected function getDeleteButton()
    {
        return GeneralUtility::makeInstance(DeleteButton::class);
    }

    /**
     * @return object|RecordLabel
     */
    protected function getRecordLabel()
    {
        return GeneralUtility::makeInstance(RecordLabel::class);
    }

    /**
     * @return object|RecordIcon
     */
    protected function getRecordIcon()
    {
        return GeneralUtility::makeInstance(RecordIcon::class);
    }

}
