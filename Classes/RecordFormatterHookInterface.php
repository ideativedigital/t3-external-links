<?php
declare(strict_types=1);
namespace Cobweb\ExternalLinks;

/*
 * This file is part of the Cobweb/ExternalLinks project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */


interface RecordFormatterHookInterface
{
    /**
     * Decides whether to display the edit button or not, possibly based on the record information.
     *
     * @param array $linkInformation
     * @return bool
     */
    public function displayEditButton(array $linkInformation): bool;

    /**
     * Decides whether to display the delete button or not, possibly based on the record information.
     *
     * @param array $linkInformation
     * @return bool
     */
    public function displayDeleteButton(array $linkInformation): bool;
}