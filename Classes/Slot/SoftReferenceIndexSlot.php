<?php

declare(strict_types=1);

namespace Cobweb\ExternalLinks\Slot;

/*
 * This file is part of the Cobweb/ExternalLinks project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Database\SoftReferenceIndex;

/**
 * Class for reacting to signals from the TYPO3\CMS\Core\Database\SoftReferenceIndex class.
 *
 * @package Cobweb\ExternalLinks\Slot
 */
class SoftReferenceIndexSlot
{
    /**
     * Enriches element data, if link is of type "externalLink".
     *
     * @param bool $linkHandlerFound
     * @param array $linkParts
     * @param string $content
     * @param array $elements
     * @param int $idx
     * @param string $tokenID
     * @param SoftReferenceIndex $index
     * @return array Modified arguments
     */
    public function setElementData(bool $linkHandlerFound, array $linkParts, string $content, array $elements, int $idx, string $tokenID, SoftReferenceIndex $index): array
    {
        // If the type is "externalLink", we have a match and define some information
        if ($linkParts['type'] === 'externalLink') {
            $linkHandlerFound = true;
            $elements[$tokenID . ':' . $idx]['subst'] = [
                'type' => 'string',
                'recordRef' => 'tx_externallinks_domain_model_externallink:' . $linkParts['uid'],
                'tokenID' => $tokenID,
                'tokenValue' => $content
            ];
        }
        return [$linkHandlerFound, $linkParts, $content, $elements, $idx, $tokenID, $index];
    }
}
