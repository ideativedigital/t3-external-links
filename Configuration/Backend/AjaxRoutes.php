<?php

return [
    'external_link_action_save' => [
        'path' => '/external/link/save',
        'target' => \Cobweb\ExternalLinks\Controller\ExternalLinkAjaxController::class . '::saveAction'
    ],
    'external_link_action_list' => [
        'path' => '/external/link/list',
        'target' => \Cobweb\ExternalLinks\Controller\ExternalLinkAjaxController::class . '::listAction'
    ],
    'external_link_action_delete' => [
        'path' => '/external/link/delete',
        'target' => \Cobweb\ExternalLinks\Controller\ExternalLinkAjaxController::class . '::deleteAction'
    ]
];