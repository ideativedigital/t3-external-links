# External Links

This is a TYPO3 CMS extension to handle external links within the new
LinkHandler popup, introduced in the version 8 of TYPO3 CMS. The extension
provides a basic table to store the external links. The extension displays the
basic functionalities to create / read / update / delete the external links.

![](https://raw.githubusercontent.com/cobwebch/external_links/master/Documentation/popup-01.png)

## Link Validator

The extension integrates itself with the LinkValidator extension. There is
nothing special to do. A new LinkType will be declared when installing the
extension and will display a new entry in the LinkValidator BE module.  

![](https://raw.githubusercontent.com/cobwebch/external_links/master/Documentation/link-validator-01.png)

## Hooks

In the link browser, each external link entry appears with an edit and a delete
button. It is possible to influence the visibility of these buttons
by using a hook.

The hook must be declared using:

`$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['external_links']['recordFormatter'] = 'some class name';`

and the class must implement the `\Cobweb\ExternalLinks\RecordFormatterHookInterface`. Note
that this means you must implement both methods, even if you want to influence
only one button (just always send back `true` for the other).

## Configuration

We can adjust some configuration to control the error types that will be reported.

```
mod.linkvalidator.externalLink {
  # Filter http status code 300. Default is "1" as redirections considered OK.
  filterCode300 = 1

  # Regular expression to filter possible errors that are not relevant to be reported by the link validator
  errorFilters {
    # This is not a critical error
    # Regular expression
    0 = SSL certificate problem: unable to get local issuer certificate
  }
}
```
