/*
 * This file is part of the Cobweb/ExternalLinks project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

/**
 * Module: Cobweb/ExternalLinks/ExternalLinkHandler
 */
define([
    'jquery',
    'TYPO3/CMS/Recordlist/LinkBrowser',
    'TYPO3/CMS/Backend/Modal',
    'TYPO3/CMS/Backend/Severity',
    'TYPO3/CMS/ExternalLinks/LocalizationUtility',
    'TYPO3/CMS/Backend/Notification',
    'tablesort'
], function($, LinkBrowser, Modal, Severity, LocalizationUtility, Notification) {
    'use strict';

    /**
     * @type {{}}
     * @exports Cobweb/ExternalLinks/ExternalLinkHandler
     */
    var ExternalLinkHandler;

    ExternalLinkHandler = {
        /**
         * Keep reference of the dataTable
         */
        dataTable: null,

        /**
         * Keep reference of the modal window
         */
        modal: null,

        /**
         * Keep reference of the current create/update form
         */
        form: null,

        /**
         * Keep reference of the grid
         */
        grid: null,

        /**
         * Initialize the View
         */
        initialize: function() {

            this.form = $('#form-external-link');
            this.grid = $('#table-external-links');

            ExternalLinkHandler.initializeGrid();
            ExternalLinkHandler.initializeSearch();
            ExternalLinkHandler.initializeEditButton();
            ExternalLinkHandler.initializeDeleteButton();
            ExternalLinkHandler.initializeSetLinkAndCloseWindow();
            ExternalLinkHandler.initializeForm();
        },

        /**
         * Initialize the Grid.
         */
        initializeGrid: function() {

            this.dataTable = this.grid.DataTable({
                paging: false,
                info: false,
                language: {
                    url: "../typo3conf/ext/external_links/Resources/Public/JavaScript/Lang/datatables." + $('html').attr('lang') + ".json"
                },
                ajax: {
                    url: TYPO3.settings.ajaxUrls['external_link_action_list'],
                    data: function ( d ) {
                        d.search = ExternalLinkHandler.urlencode($('#table-external-links_filter').find('input[type="search"]').first().val());
                        // Retrieve current uid from title
                        let matches = $(".element-browser-title").clone().children().remove().end().text().match(/\(([^)]+)\)/);
                        d.uid = (matches != null ? matches[1] : 0);
                    }
                },
                "processing": true,
                columns: [
                    {data: 'icon'},
                    {data: 'label'},
                    {data: 'note'},
                    {data: 'commands'}
                ]
            });
        },

        /**
         * Initialize the "new"/"edit" form
         * Action when the "new"/"edit" form is submitted
         */
        initializeForm: function() {

            this.form.on('submit', function(event) {
                event.preventDefault();
                $('#table-external-links_filter input[type="search"]').val('');

                // We first validate the form
                if (ExternalLinkHandler.validate('url')) {
                    $.ajax({
                        url: TYPO3.settings.ajaxUrls['external_link_action_save'],
                        method: 'POST',
                        data: $(this).serialize(),
                        success: function(response) {
                            // Load the newly created link as a search string so that it appears in the list
                            if (response) {
                                $('#table-external-links_filter input[type="search"]').val(response.url);
                            }

                            // Reload the Grid and act when done
                            ExternalLinkHandler.dataTable.ajax.reload(function () {

                                // Grid is not in a loading state anymore.
                                ExternalLinkHandler.setIsLoading(false);

                                // Reset form for a new link.
                                ExternalLinkHandler.resetForm();

                                if (response) {
                                    response = JSON.parse(response)
                                    if (response && response.url) {
                                        if (response.action === 'create') {
                                            Notification.success(LocalizationUtility.localize('message.add.success'), response.url);
                                        } else {
                                            Notification.success(LocalizationUtility.localize('message.edit.success'), response.url);
                                        }

                                    } else {
                                        Notification.error(LocalizationUtility.localize('message.error'), '');
                                    }
                                }
                            });
                        }
                    });
                }
            });
            // Cancel button
            this.form.find('#form-external-link_cancel').on('click', function () {
                ExternalLinkHandler.resetForm();
            });
        },

        resetForm: function() {
            ExternalLinkHandler.form.get(0).reset();
            ExternalLinkHandler.form.find('#form-external-link_new').show();
            ExternalLinkHandler.form.find('#form-external-link_save').hide();
            ExternalLinkHandler.form.find('#form-external-link_cancel').hide();
        },

        /**
         * Initialize the search field, which should react to any form of input
         */
        initializeSearch: function() {
            $('body').on('input', '#table-external-links_filter input[type="search"]', function() {
                ExternalLinkHandler.dataTable.ajax.reload(function() {
                });
            });
        },

        /**
         * Initialize the edit button
         * Action when a link is clicked => close the popup and inject the value into the field.
         */
        initializeSetLinkAndCloseWindow: function() {
            $(document).on('click', '.container-external-link-label a', function(e) {
                e.preventDefault();
                LinkBrowser.finalizeFunction('t3://externalLink?uid=' + $(this).data('uid'));
            })
        },

        /**
         * Initialize the edit button
         * Action when the edit button is clicked.
         */
        initializeEditButton: function() {

            $(document).on('click', '.btn-edit', function(e) {
                e.preventDefault();

                let linkData = {
                    url: $(this).data('url'),
                    note: $(this).data('note'),
                    uid: $(this).data('uid')
                }
                ExternalLinkHandler.form.find('#field-uid').val(linkData.uid);
                ExternalLinkHandler.form.find('#field-url').val(linkData.url);
                ExternalLinkHandler.form.find('#field-note').val(linkData.note);
                ExternalLinkHandler.form.find('#form-external-link_new').hide();
                ExternalLinkHandler.form.find('#form-external-link_save').show();
                ExternalLinkHandler.form.find('#form-external-link_cancel').show();
            });
        },

        /**
         * Initialize the delete button
         * Action when the delete button is clicked.
         */
        initializeDeleteButton: function() {

            $(document).on('click', '.btn-delete', function(e) {
                e.preventDefault();

                var linkIdentifier = $(this).data('uid'),
                    linkUrl = $(this).data('url');

                Modal.confirm(
                    LocalizationUtility.localize('modal.delete'),
                    LocalizationUtility.localize('modal.delete.sure?', {0: linkUrl, 1: linkIdentifier}),
                    Severity.warning,
                    [
                        {
                            text: LocalizationUtility.localize('action.cancel'),
                            btnClass: 'btn btn-default',
                            trigger: function() {
                                Modal.dismiss();
                            }
                        },
                        {
                            text: LocalizationUtility.localize('action.delete'),
                            btnClass: 'btn btn-warning',
                            trigger: function() {

                                // Grid is not in a loading state anymore.
                                ExternalLinkHandler.setIsLoading(true);

                                $.ajax({
                                    url: TYPO3.settings.ajaxUrls['external_link_action_delete'],
                                    method: 'POST',
                                    data: {uid: linkIdentifier},
                                    success: function(response) {

                                        // Reload the Grid.
                                        ExternalLinkHandler.dataTable.ajax.reload();

                                        // Grid is not in a loading state anymore.
                                        ExternalLinkHandler.setIsLoading(false);

                                        // Close modal window
                                        Modal.dismiss();
                                    }
                                })
                            }
                        }
                    ]
                );
            });
        },

        /**
         * Set the Grid as loading state
         */
        setIsLoading: function(isLoading) {
            if (isLoading) {
                this.form.find('input, button').attr('disabled', '');
                this.grid.css({opacity: 0.4});
            } else {
                this.form.find('input, button').removeAttr('disabled');
                this.grid.css({opacity: 1});
            }
        },

        /**
         * Validate
         */
        validate: function(field) {
            hasError = true;
            if (field === 'url') {
                var $input, hasError;
                $input = $('#field-url');

                hasError = !/^(http(s?))\:\/\//.test($input.val());
                if (hasError) {
                    $input.parent('div').addClass('has-error');
                } else {
                    $input.parent('div').removeClass('has-error');
                }
            }
            return !hasError;
        },

        /**
         * JS Equivalence of PHP urlencode
         */
        urlencode: function (str) {
            str = (str + '')
            return encodeURIComponent(str)
                .replace(/!/g, '%21')
                .replace(/'/g, '%27')
                .replace(/\(/g, '%28')
                .replace(/\)/g, '%29')
                .replace(/\*/g, '%2A')
                .replace(/%20/g, '+')
        }
    };

    // After the DOM is loaded... time to react!
    $(function() {
        ExternalLinkHandler.initialize()
    });

    return ExternalLinkHandler;
});