<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'External Links',
	'description' => 'Extension to handle external links in a centralized way',
	'category' => 'plugin',
	'author' => 'Fabien Udriot',
	'author_email' => 'fabien.udriot@cobweb.ch',
	'state' => 'stable',
	'clearCacheOnLoad' => 0,
	'version' => '11.0.0',
    'autoload' => [
        'psr-4' => ['Cobweb\\ExternalLinks\\' => 'Classes']
    ],
	'constraints' => [
		'depends' => [
			'typo3' => '11.5.0-11.99.99',
        ],
		'conflicts' => [
        ],
		'suggests' => [
        ],
    ],
];
